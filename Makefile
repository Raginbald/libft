# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/26 19:01:09 by graybaud          #+#    #+#              #
#    Updated: 2016/03/05 12:33:46 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
NAME 	= libft.a
H		= libft.h
CC		= clang
CFLAGS	= -Wall -Wextra -Werror -I./
RM		= rm -rf
SRC		= ft_atoi.c				\
          ft_abs.c 				\
		  ft_reverse_str.c		\
		  ft_strlen.c			\
		  ft_strcpy.c			\
		  ft_strncpy.c			\
		  ft_strdup.c			\
		  ft_strcat.c			\
		  ft_strlcat.c			\
		  ft_strncat.c			\
		  ft_strchr.c			\
		  ft_strrchr.c			\
		  ft_strstr.c			\
		  ft_strnstr.c			\
		  ft_isalpha.c			\
		  ft_isdigit.c			\
		  ft_isalnum.c			\
		  ft_isascii.c			\
		  ft_islowercase.c		\
		  ft_isprint.c			\
		  ft_isnegative.c		\
		  ft_itoa.c				\
		  ft_isuppercase.c		\
		  ft_iswhitespace.c		\
		  ft_toupper.c			\
		  ft_tolower.c			\
		  ft_strcmp.c			\
		  ft_strncmp.c			\
		  ft_memcmp.c			\
		  ft_bzero.c			\
		  ft_min.c 				\
		  ft_max.c 				\
		  ft_memset.c			\
		  ft_memcpy.c			\
		  ft_memccpy.c			\
		  ft_memmove.c			\
		  ft_memchr.c			\
		  ft_memalloc.c			\
		  ft_memdel.c			\
		  ft_strnew.c			\
		  ft_strdel.c			\
		  ft_strclr.c			\
		  ft_striter.c			\
		  ft_striteri.c			\
		  ft_strmap.c			\
		  ft_strmapi.c			\
		  ft_strequ.c			\
		  ft_strnequ.c			\
		  ft_strsub.c			\
		  ft_strjoin.c			\
		  ft_strtrim.c			\
		  ft_strsplit.c			\
		  ft_putchar.c			\
		  ft_putchar_fd.c		\
		  ft_putendl.c			\
		  ft_putstr.c			\
		  ft_putnbr.c			\
		  ft_putstr_fd.c		\
		  ft_putendl_fd.c		\
		  ft_putnbr_fd.c		\
		  ft_list_foreach.c		\
		  ft_lstnew.c			\
		  ft_lstadd.c			\
		  ft_lstdel.c			\
		  ft_lstdelone.c		\
		  ft_lstiter.c			\
		  ft_lstmap.c 			\
		  ft_realloc.c			\
		  ft_get_next_line.c 	\
		  ft_epur_str.c 		\

OBJ =	$(SRC:.c=.o)

$(NAME): $(OBJ) $(H)
	@ar -q $(NAME) $(OBJ)
	@ranlib $(NAME)

$(%.o): %.c
	@$(CC) $(CFLAGS) -o $@ -c $<

all: $(NAME)

clean:
	@$(RM) $(OBJ)

fclean:		clean
	@$(RM) $(NAME)

re:	fclean all

.PHONY: all, clean, fclean, re
