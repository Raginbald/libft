/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_word.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amorfan <amorfan@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/26 19:33:16 by amorfan           #+#    #+#             */
/*   Updated: 2014/01/26 19:33:20 by amorfan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_count_word(char const *s)
{
	int		word;
	int		i;

	i = 0;
	word = 0;
	while (s[i])
	{
		while (s[i] && ft_iswhitespace(s[i]))
			i++;
		while (s[i] && !ft_iswhitespace(s[i]))
			i++;
		word++;
		while (s[i] && ft_iswhitespace(s[i]))
			i++;
	}
	return (word);
}
