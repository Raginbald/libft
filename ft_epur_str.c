/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_epur_str.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/01 12:06:24 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/01 17:11:32 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_epur_str(char *s)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (s[i])
	{
		while (ft_iswhitespace(s[i]) && s[i])
			i++;
		while (!ft_iswhitespace(s[i]) && s[i])
			s[j++] = s[i++];
		if (!s[i])
			break ;
		if (ft_iswhitespace(s[i]))
			s[j++] = ' ';
	}
	if (ft_iswhitespace(s[j - 1]))
		s[j - 1] = '\0';
	else
		s[j] = '\0';
	return (s);
}
