/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 17:22:44 by rthebaud          #+#    #+#             */
/*   Updated: 2013/12/22 17:34:31 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int	ft_format_pad(t_parse *p, size_t size)
{
	int	i;
	int	pad;

	i = 0;
	pad = p->pad - size;
	if (pad > 0)
	{
		while (i < pad)
		{
			if (!(p->flags & FLAG_MIN) && p->flags & FLAG_0 && p->prec == -1)
				ft_putchar(C_0);
			else
				ft_putchar(C_SPC);
			i++;
		}
	}
	return (i);
}
