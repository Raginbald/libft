/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_char.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 14:08:14 by rthebaud          #+#    #+#             */
/*   Updated: 2013/12/22 17:58:55 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			ft_format_char(va_list args, t_parse *p)
{
	int	c;
	int	i;

	c = va_arg(args, int);
	i = 1;
	if (!(p->flags & FLAG_MIN))
		i += ft_format_pad(p, sizeof(char));
	ft_putchar(c);
	if ((p->flags & FLAG_MIN))
		i += ft_format_pad(p, sizeof(char));
	return (i);
}
