/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_number.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 15:54:42 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/08 17:19:15 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static void	_putsign(t_parse *p, int n)
{
	if (n < 0)
		ft_putchar(C_MIN);
	else if (p->flags & FLAG_PLUS)
		ft_putchar(C_PLUS);
	else if (p->flags & FLAG_SPC)
		ft_putchar(C_SPC);
}

int			ft_format_prec(t_parse *p, int size)
{
	int	prec;

	prec = 0;
	if (!(p->flags & FLAG_MIN) && p->prec == -1
		&& p->flags & FLAG_0 && p->pad > size)
		p->prec = p->pad - 1;
	if (p->prec != -1 && p->prec > size)
		prec = p->prec - size;
	return (prec);
}

int			ft_format_number(va_list args, t_parse *p)
{
	int		n;
	int		i;
	int		size;
	int		prec;

	n = va_arg(args, int);
	i = 0;
	size = ft_declen(n);
	prec = ft_format_prec(p, size);
	size += prec;
	if (n < 0 || p->flags & FLAG_SPC || p->flags & FLAG_PLUS)
		size++;
	if (!(p->flags & FLAG_MIN))
		i += ft_format_pad(p, size);
	_putsign(p, n);
	while (prec--)
		ft_putchar(C_0);
	ft_putdec(n);
	i += size;
	if ((p->flags & FLAG_MIN))
		i += ft_format_pad(p, size);
	return (i);
}
