/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_string.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 14:47:53 by rthebaud          #+#    #+#             */
/*   Updated: 2013/12/22 18:00:16 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			ft_format_string(va_list args, t_parse *p)
{
	char	*s;
	int		i;
	int		j;
	int		size;

	s = va_arg(args, char*);
	i = 0;
	size = ft_strlen(s);
	if (p->prec != -1 && p->prec < size)
		size = p->prec;
	if (!(p->flags & FLAG_MIN))
		i += ft_format_pad(p, size);
	j = 0;
	while (j < size && *s)
	{
		ft_putchar(*s++);
		i++;
		j++;
	}
	if ((p->flags & FLAG_MIN))
		i += ft_format_pad(p, size);
	return (i);
}
