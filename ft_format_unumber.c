/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_unumber.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 17:17:44 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/08 17:19:25 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			ft_format_unumber(va_list args, t_parse *p)
{
	unsigned int	n;
	int				i;
	size_t			size;
	int				prec;

	n = va_arg(args, unsigned int);
	i = 0;
	size = ft_declen(n);
	prec = ft_format_prec(p, size);
	size += prec;
	if (!(p->flags & FLAG_MIN))
		i += ft_format_pad(p, size);
	while (prec--)
		ft_putchar(C_0);
	ft_putdec(n);
	i += size;
	if ((p->flags & FLAG_MIN))
		i += ft_format_pad(p, size);
	return (i);
}
