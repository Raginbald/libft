/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ftoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/17 10:49:21 by vincent           #+#    #+#             */
/*   Updated: 2013/12/20 22:31:31 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_ftoa(float n)
{
	char	*ret;
	char	*tmp;
	int		i;

	i = (int)n;
	ret = ft_itoa(i);
	if (n < 0)
		n = -n;
	n = n - (float)i;
	if (n == 0)
		return (ret);
	tmp = ret;
	ret = ft_strjoin(ret, ".");
	ft_strdel(&tmp);
	while (n - (float)i != 0)
	{
		n = n * 10;
		i = (int)n;
	}
	tmp = ret;
	ret = ft_strjoin(ret, ft_itoa(i));
	ft_strdel(&tmp);
	return (ret);
}
