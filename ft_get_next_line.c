/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_next_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 22:30:20 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/01 17:01:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <libft.h>

static void		ft_check_end_line(char *offset, int ret)
{
	if (!offset)
		return ;
	if (ret != 0)
	{
		offset[ret++] = '\n';
		offset[ret++] = '\n';
		offset[ret] = '\0';
	}
}

static int		ft_free_gnl(int ret)
{
	if (ret == -1)
		return (-1);
	if (ret == 0)
		return (0);
	return (0);
}

int				ft_get_next_line(int const fd, char **line)
{
	static char *offset = NULL;
	int			ret;
	int			i;

	i = 0;
	if (fd < 0 || BUFF_SIZE < 1 || !(*line = ft_memalloc(BUFF_SIZE + 1)))
		return (-1);
	ret = read(fd, *line, BUFF_SIZE);
	offset = ft_strjoin(offset, *line);
	ft_strclr(*line);
	if (ret == -1)
		return (ft_free_gnl(ret));
	ft_check_end_line(offset, ret);
	while (offset[i] != '\n')
		i++;
	offset[i] = '\0';
	*line = ft_strdup(offset);
	if (offset[i + 1] != '\0')
	{
		offset = &offset[i + 1];
		return (1);
	}
	return (ft_free_gnl(ret));
}
