/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isuppercase.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/11 22:21:19 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/12 15:59:13 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_isuppercase(int c)
{
	return (c <= 90 && c >= 65);
}
