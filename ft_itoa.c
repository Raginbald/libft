/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 12:55:49 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/16 11:13:30 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int			ft_intlen(long int c)
{
	size_t			len;

	len = 0;
	if (c <= 0)
		len = len + 1;
	while (c)
	{
		c = c / 10;
		len = len + 1;
	}
	return (len);
}

static char			*ft_write_int(char *copy, long int c, size_t len)
{
	size_t			i;

	i = len - 1;
	if (c < 0)
	{
		copy[0] = '-';
		c = -c;
	}
	if (c == 0)
		copy[0] = '0';
	while (c)
	{
		copy[i] = c % 10 + '0';
		c = c / 10;
		i--;
	}
	return (copy);
}

char				*ft_itoa(int c)
{
	size_t			len;
	char			*copy;

	len = ft_intlen((long int)c);
	copy = ft_strnew((size_t)len);
	copy = ft_write_int(copy, (long int)c, len);
	return (copy);
}
