/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_unsign.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/19 14:42:32 by vvaleriu          #+#    #+#             */
/*   Updated: 2014/01/18 16:16:57 by vvaleriu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int		mal_size(unsigned int n);
static int		pow_len(unsigned int n);
static int		power(unsigned int n, int pow);
static void		fill(char *s, int *i, unsigned int *val, int *len);

char			*ft_itoa_unsign(unsigned int n)
{
	char	*str;
	int		len;
	int		i;

	i = 0;
	len = pow_len(n);
	str = (char *) ft_memalloc (sizeof(char) * mal_size(n) + 1);
	if (!str)
		return (NULL);
	fill(str, &i, &n, &len);
	return (str);
}

static int		mal_size(unsigned int n)
{
	int	size;

	size = 1;
	while (n > 10)
	{
		n = n / 10;
		size++;
	}
	return (size);
}

static int		pow_len(unsigned int n)
{
	int	size;

	size = 0;
	while (n >= 10)
	{
		n = n / 10;
		size++;
	}
	return (size);
}

static int		power(unsigned int n, int pow)
{
	int		a;

	a = n;
	if (pow == 0)
		return (1);
	while (pow > 1)
	{
		a = n * a;
		pow--;
	}
	return (a);
}

static void		fill(char *str, int *i, unsigned int *n, int *len)
{
	while (*len >= 0)
	{
		str[*i] = *n / power(10, *len) + 48;
		(*i)++;
		*n = *n % power(10, *len);
		if (*n == 0)
		{
			while (*len)
			{
				str[*i] = 48;
				(*i)++;
				(*len)--;
			}
		}
		(*len)--;
	}
	str[*i] = '\0';
}
