/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoh_cap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vincent <vincent@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 23:13:18 by vincent           #+#    #+#             */
/*   Updated: 2014/03/27 02:55:42 by vincent          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Stocke dans un tableau les valeurs hexadecimales correspondantes aux valeurs
** decimales (ex : hex[12] = C, car C = 12 en hexadecimal)
** Compare bit a bit les 4 derniers bits de l'entier recu. Cela donne une valeur
** entre 0 et 15. On dit que notre tbleau de retour prend la valeur de la case
** issue de cette comparaison.
*/

char		*ft_itoh_cap(unsigned int n)
{
	char 	hex[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'\
		, 'A', 'B', 'C', 'D', 'E', 'F'};
	char	*ret;
	int		len;

	len = 0;
	ret = ft_strnew(16);
	while (n != 0)
	{
		ret[len] = hex[n & 0x000F];
		len++;
		n = (n >> 4);
	}
	ft_strrev(ret);
	return (ret);
}
