/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_foreach.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 12:16:57 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:16:59 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	ft_list_foreach(t_list *node, void (*f)(void *))
{
	if (node)
	{
		f(&node->content);
		if (node->next)
			ft_list_foreach(node->next, f);
	}
}
