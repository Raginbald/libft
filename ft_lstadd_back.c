/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 14:03:09 by graybaud          #+#    #+#             */
/*   Updated: 2014/02/19 14:22:18 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void		ft_lstadd_back(t_list **head, void	*data)
{
	t_list	*i;
	t_list	*last;

	i = *head;
	last = ft_lstnew(data);
	if (*head)
	{
		while (i->next)
			i = i->next;
		i->next = last;
	}
	else
		*head = last;
}
