/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_front.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 19:53:46 by graybaud          #+#    #+#             */
/*   Updated: 2014/03/03 14:26:05 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void		ft_lstadd_front(t_list **head, void	*data)
{
	t_list	*new;

	new = ft_lstnew(data);
	new->next = *head;
	*head = new;
}
