/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstapd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 21:16:27 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/07 09:50:05 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_list.h"

void	ft_lstapd(t_list **alst, t_list *new)
{
	t_list	*item;

	item = *alst;
	if (alst && item)
	{
		while (item->next)
		{
			item = item->next;
		}
		item->next = new;
	}
	else
	{
		*alst = new;
	}
}
