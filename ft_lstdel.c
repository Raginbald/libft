/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/19 12:23:52 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/19 12:43:33 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static void		ft_lstdel_aux(t_list *alst, void (*del)(void *, size_t))
{
	if (alst)
	{
		ft_lstdel_aux(alst->next, del);
		ft_lstdelone(&alst, del);
	}
}

void			ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	ft_lstdel_aux(*alst, del);
	*alst = NULL;
}
