/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/19 13:33:33 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/19 15:53:59 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*copy;
	t_list	*temp;

	copy = NULL;
	if (lst)
	{
		temp = f(lst);
		copy = ft_lstnew(temp->content, temp->content_size);
		copy->next = ft_lstmap(lst->next, f);
	}
	return (copy);
}
