/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 18:48:33 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/19 15:35:42 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include <libft.h>

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*t;

	if (!(t = (t_list *)malloc(sizeof(t_list))))
		return (NULL);
	if (!content)
	{
		content = NULL;
		content_size = 0;
	}
	else
	{
		t->content = (void *)content;
		t->content_size = content_size;
		t->next = NULL;
	}
	return (t);
}
