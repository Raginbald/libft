/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnewref.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 06:43:15 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/07 09:50:05 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_list.h"

t_list	*ft_lstnewref(void *content, size_t content_size)
{
	t_list		*item;

	if ((item = ft_memalloc(sizeof(t_list))))
	{
		if (content)
		{
			item->content = content;
			item->content_size = content_size;
		}
		else
		{
			item->content = 0;
			item->content_size = 0;
		}
		item->next = 0;
	}
	return (item);
}
