/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 13:59:00 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/04 15:02:11 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

void		*ft_memalloc(size_t size)
{
	char	*ptr;

	if (!(ptr = (char *)malloc(sizeof(char *) * size)))
		return (NULL);
	ft_bzero(ptr, size);
	return ((void *)ptr);
}
