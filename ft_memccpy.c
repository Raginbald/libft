/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:41:45 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/16 10:58:15 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void				*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	unsigned char	*ptr_s1;
	unsigned char	*ptr_s2;
	size_t			i;

	ptr_s1 = (unsigned char *)s1;
	ptr_s2 = (unsigned char *)s2;
	i = 0;
	while (n)
	{
		if (ptr_s2[i] == (unsigned char)c)
		{
			ptr_s1[i] = ptr_s2[i];
			return ((void *)(ptr_s1 + i + 1));
		}
		else
			ptr_s1[i] = ptr_s2[i];
		i++;
		n--;
	}
	return (NULL);
}
