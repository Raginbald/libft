/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:42:07 by graybaud          #+#    #+#             */
/*   Updated: 2014/01/05 20:32:03 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int			ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*ptr_s1;
	unsigned char	*ptr_s2;

	ptr_s1 = (unsigned char *)s1;
	ptr_s2 = (unsigned char *)s2;
	while (n)
	{
		if (*ptr_s1 != *ptr_s2)
			return (*(unsigned char *)ptr_s1 - *(unsigned char *)ptr_s2);
		ptr_s1++;
		ptr_s2++;
		n--;
	}
	return (0);
}
