/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:42:36 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/16 11:00:00 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void			*ft_memmove(void *s1, const void *s2, size_t n)
{
	char		*ptr_s1;
	char		*ptr_s2;

	ptr_s1 = (char *)s1;
	ptr_s2 = (char *)s2;
	if (ptr_s2 <= ptr_s1)
	{
		ptr_s1 += n - 1;
		ptr_s2 += n - 1;
		while (n--)
			*ptr_s1-- = *ptr_s2--;
	}
	else
		ft_memcpy(s1, s2, n);
	return ((void *)s1);
}
