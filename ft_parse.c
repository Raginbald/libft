/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 08:15:37 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/08 17:19:59 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static t_parse	*_new_parse(void)
{
	t_parse	*p;

	if (!(p = ft_memalloc(sizeof(t_parse))))
		return (NULL);
	p->flags = FLAG_NONE;
	p->lm = LM_NONE;
	p->sp = SP_NONE;
	p->pad = 0;
	p->prec = -1;
	return (p);
}

static int		_parse_flags(char **str, t_parse *p)
{
	int	match;

	match = 1;
	if (**str == C_SHARP)
		p->flags |= FLAG_SHARP;
	else if (**str == C_0)
		p->flags |= FLAG_0;
	else if (**str == C_MIN)
		p->flags |= FLAG_MIN;
	else if (**str == C_SPC)
		p->flags |= FLAG_SPC;
	else if (**str == C_PLUS)
		p->flags |= FLAG_PLUS;
	else if (**str == C_QUO)
		p->flags |= FLAG_QUO;
	else
		match = 0;
	if (match)
		(*str)++;
	return (match);
}

static int		_parse_pat(char **str)
{
	if (**str == C_PAT)
	{
		ft_putchar(C_PAT);
		(*str)++;
		return (1);
	}
	return (0);
}

int				_parse(va_list args, t_parse *p)
{
	if (p->sp == SP_c)
		return (ft_format_char(args, p));
	if (p->sp == SP_s)
		return (ft_format_string(args, p));
	if (p->sp == SP_d || p->sp == SP_i
			|| p->sp == SP_o || p->sp == SP_x || p->sp == SP_X)
		return (ft_format_number(args, p));
	if (p->sp == SP_u)
		return (ft_format_unumber(args, p));
	return (0);
}

int				ft_parse(char **str, va_list args)
{
	t_parse	*p;
	int		match;

	if (!str)
		return (0);
	(*str)++;
	if (_parse_pat(str))
		return (1);
	p = _new_parse();
	while (**str)
	{
		match = 0;
		match = match || _parse_flags(str, p);
		match = match || ft_parse_prec(str, p, args);
		match = match || ft_parse_pad(str, p, args);
		match = match || ft_parse_lm(str, p);
		if (!match && ft_parse_sp(str, p))
			break ;
		if (!match)
			return (0);
	}
	match = _parse(args, p);
	ft_memdel((void**)&p);
	return (match);
}
