/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_lm.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 11:42:39 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/08 17:20:15 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static char const	**_ft_parse_lm(size_t *size)
{
	static char const	*lm_tab[] = {
		[LM_hh] = C_LM_hh,
		[LM_h] = C_LM_h,
		[LM_l] = C_LM_l,
		[LM_ll] = C_LM_ll,
		[LM_j] = C_LM_j,
		[LM_t] = C_LM_t,
		[LM_z] = C_LM_z,
		[LM_q] = C_LM_q,
		[LM_L] = C_LM_L,
		[LM_v] = C_LM_v,
		[LM_vh] = C_LM_vh,
		[LM_hv] = C_LM_hv,
		[LM_vl] = C_LM_vl,
		[LM_lv] = C_LM_lv,
		[LM_vll] = C_LM_vll,
		[LM_llv] = C_LM_llv
	};
	static size_t const	lm_tab_size = sizeof(lm_tab) / sizeof(char*);

	*size = lm_tab_size;
	return (lm_tab);
}

int					ft_parse_lm(char **str, t_parse *p)
{
	size_t		lm;
	char const	**lm_tab;
	size_t		lm_tab_size;
	size_t		size;

	if (!str || !*str || !**str)
		return (0);
	lm_tab = _ft_parse_lm(&lm_tab_size);
	lm = LM_NONE + 1;
	while (lm < lm_tab_size)
	{
		size = sizeof(lm_tab[lm] - 1);
		if (ft_strnequ(*str, lm_tab[lm], size))
		{
			p->lm = lm;
			(*str) += size;
			return (1);
		}
		lm++;
	}
	return (0);
}
