/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_pad.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 11:18:37 by rthebaud          #+#    #+#             */
/*   Updated: 2013/12/22 11:27:49 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		ft_parse_pad(char **str, t_parse *p, va_list args)
{
	int	pad;

	if (!str || !*str || !**str)
		return (0);
	if (**str == C_ARG)
	{
		p->pad = va_arg(args, int);
		(*str)++;
		return (1);
	}
	if (!ft_isdigit(**str) || **str == '0')
		return (0);
	pad = 0;
	while (ft_isdigit(**str))
	{
		pad *= BASE_DEC;
		pad = pad + **str - '0';
		(*str)++;
	}
	p->pad = pad;
	return (1);
}
