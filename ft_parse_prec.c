/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_prec.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 10:54:15 by rthebaud          #+#    #+#             */
/*   Updated: 2013/12/22 11:16:52 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		ft_parse_prec(char **str, t_parse *p, va_list args)
{
	int	prec;

	if (!str || !*str || !**str || **str != C_PREC)
		return (0);
	(*str)++;
	if (**str == C_ARG)
	{
		p->prec = va_arg(args, int);
		(*str)++;
		return (1);
	}
	prec = 0;
	while (ft_isdigit(**str))
	{
		prec *= BASE_DEC;
		prec = prec + **str - '0';
		(*str)++;
	}
	p->prec = prec;
	return (1);
}
