/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_sp.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 12:06:45 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/08 17:20:43 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

static char const	*_ft_parse_sp(size_t *size)
{
	static char const	sp_tab[] = {
		[SP_d] = C_SP_d, [SP_D] = C_SP_D, [SP_i] = C_SP_i,
		[SP_o] = C_SP_o, [SP_O] = C_SP_O, [SP_u] = C_SP_u, [SP_U] = C_SP_U,
		[SP_x] = C_SP_x, [SP_X] = C_SP_X, [SP_n] = C_SP_n, [SP_a] = C_SP_a,
		[SP_A] = C_SP_A, [SP_e] = C_SP_e, [SP_E] = C_SP_E, [SP_f] = C_SP_f,
		[SP_F] = C_SP_F, [SP_g] = C_SP_g, [SP_G] = C_SP_G, [SP_c] = C_SP_c,
		[SP_C] = C_SP_C, [SP_S] = C_SP_S, [SP_s] = C_SP_s, [SP_p] = C_SP_p
	};
	static size_t const	sp_tab_size = sizeof(sp_tab) / sizeof(sp_tab[0]);

	*size = sp_tab_size;
	return (sp_tab);
}

int					ft_parse_sp(char **str, t_parse *p)
{
	size_t		sp;
	char const	*sp_tab;
	size_t		sp_tab_size;

	if (!str || !*str || !**str)
		return (0);
	sp_tab = _ft_parse_sp(&sp_tab_size);
	sp = SP_NONE + 1;
	while (sp < sp_tab_size)
	{
		if (**str == sp_tab[sp])
		{
			p->sp = sp;
			(*str)++;
			return (1);
		}
		sp++;
	}
	return (0);
}
