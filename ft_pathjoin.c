/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pathjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/23 11:16:01 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/23 11:18:56 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_pathjoin(char const *s1, char const *s2, char sep)
{
	char	*res;
	size_t	size;

	res = 0;
	if (s1 && s2)
	{
		size = ft_strlen(s1) + ft_strlen(s2) + 1;
		if ((res = ft_strnew(size)))
		{
			while (*s1)
			{
				*res++ = *s1++;
			}
			*res++ = sep;
			while (*s2)
			{
				*res++ = *s2++;
			}
			res -= size;
		}
	}
	return (res);
}
