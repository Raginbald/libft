/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putdec.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 16:09:52 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/07 14:48:47 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_putdec(int n)
{
	int	i;

	i = n / 10;
	if (i)
	{
		ft_putdec(n / 10);
	}
	i = n % 10;
	if (i < 0)
	{
		i *= -1;
	}
	ft_putchar(i + '0');
	return (0);
}
