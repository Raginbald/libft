/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/01 16:11:09 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/19 12:37:26 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

char	*ft_realloc(char *ptr, size_t len)
{
	char	*new_str;

	if (!(new_str = (char *)malloc(sizeof(char *) * len)))
		return (NULL);
	ft_bzero(new_str, len);
	ft_memcpy(new_str, ptr, len);
	free(ptr);
	return (new_str);
}
