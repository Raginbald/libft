/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sin.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 14:33:53 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/15 14:34:21 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_sin(double d)
{
	double	res;
	int		sign;
	int		i;

	res = d;
	i = 3;
	sign = -1;
	while (i < 5)
	{
		res = res + (sign * (ft_pow(d, i) / ft_fact(i)));
		sign = -sign;
		i += 2;
	}
	return (res);
}
