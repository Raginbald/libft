/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 14:35:42 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/15 14:41:20 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "libft.h"

double	ft_sqrt(double num)
{
	double	es1;
	double	es2;

	if (num == 0)
		return (0.0);
	if (num < 0)
		return (NAN);
	es1 = num / 2;
	es2 = (es1 + num / es1) / 2;
	while (ft_fabs(es1 - es2) > 0)
	{
		es1 = es2;
		es2 = (es1 + num / es1) / 2;
	}
	num = es2 > es1 ? es2 : es1;
	return (num);
}
