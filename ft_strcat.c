/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:43:15 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/01 12:09:31 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char			*ft_strcat(char *s1, const char *s2)
{
	char		*ptr_s1;

	ptr_s1 = s1 + ft_strlen(s1);
	ft_strcpy(ptr_s1, s2);
	return (s1);
}
