/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strealloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaize <mlaize@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 14:53:09 by mlaize            #+#    #+#             */
/*   Updated: 2013/12/05 15:47:23 by mlaize           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strealloc(char *str, size_t size)
{
	char	*newstr;
	size_t	sizebefore;

	sizebefore = 0;
	if (str != NULL)
		sizebefore = ft_strlen(str) + 1;
	newstr = malloc(sizebefore + size);
	if (newstr == NULL)
		return (NULL);
	ft_bzero(newstr, sizebefore + size);
	if (str != NULL)
	{
		ft_strcpy(newstr, str);
		free(str);
	}
	return (newstr);
}
