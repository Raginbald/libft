/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 13:20:26 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/30 11:19:27 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strjoin(char const *s1, char const *s2)
{
	char	*copy;

	if (!s1 && !s2)
		return (NULL);
	if (!s1)
		return (ft_strdup(s2));
	if (!s2)
		return (ft_strdup(s1));
	if (s1 == s2)
		return (ft_strdup(s1));
	copy = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + 1);
	copy = ft_strcpy(copy, s1);
	copy = ft_strcat(copy, s2);
	return (copy);
}
