/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:44:50 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/17 13:01:37 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	char	*ptr_s1;
	char	*ptr_s2;
	size_t	len;
	size_t	n;

	ptr_s1 = dst;
	ptr_s2 = (char *)src;
	n = size;
	while (n-- && *ptr_s1)
		ptr_s1++;
	len = ptr_s1 - dst;
	n = size - len;
	if (n == 0)
		return (len + ft_strlen(src));
	while (*ptr_s2)
	{
		if (n != 1)
		{
			*ptr_s1++ = *ptr_s2;
			n--;
		}
		ptr_s2++;
	}
	*ptr_s1 = '\0';
	return (len + (ptr_s2 - src));
}
