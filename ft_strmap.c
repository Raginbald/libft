/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 20:01:18 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/16 11:07:59 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <libft.h>

char		*ft_strmap(char const *s, char (*f)(char))
{
	char	*map;
	size_t	i;

	i = 0;
	map = ft_strnew(ft_strlen(s));
	while ((map != NULL) && s[i])
	{
		map[i] = (*f)(s[i]);
		i++;
	}
	return (map);
}
