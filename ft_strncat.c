/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:45:20 by graybaud          #+#    #+#             */
/*   Updated: 2013/12/17 13:03:05 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strncat(char *s1, const char *s2, size_t n)
{
	char	*ptr_s1;

	ptr_s1 = s1 + ft_strlen(s1);
	while ((*s2 != '\0') && n > 0)
	{
		*ptr_s1++ = *s2++;
		n--;
	}
	*ptr_s1 = '\0';
	return (s1);
}
