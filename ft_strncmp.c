/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:45:35 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/19 16:09:32 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_strncmp(const char *s1, const char *s2, int n)
{
	int		i;

	i = 0;
	if (!n)
		return (0);
	while (i < n - 1 && s1[i] == s2[i] && s1[i])
		i++;
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}
