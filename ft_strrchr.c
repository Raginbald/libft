/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:46:16 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/16 11:10:52 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strrchr(const char *s, int c)
{
	const char	*ptr_s;

	ptr_s = s + ft_strlen(s);
	while (*ptr_s != (char)c)
	{
		if (ptr_s == s)
			return (NULL);
		ptr_s--;
	}
	return ((char *)ptr_s);
}
