/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 10:26:31 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:09:39 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

static int	ft_get_nb(const char *s, const char c)
{
	int	nb;
	int	i;

	nb = 0;
	i = 0;
	while (s[i])
	{
		while (s[i] && s[i] == c)
			i++;
		while (s[i] && s[i] != c)
			i++;
		nb++;
	}
	return (nb);
}

static int	ft_get_len(const char *s, const char c)
{
	int	i;

	i = 0;
	while (s[i] && s[i] != c)
		i++;
	return (i);
}

static char	**ft_strnew_2d(int len)
{
	char	**str;

	if (!(str = (char **)malloc(sizeof(char *) * len + 1)))
		return (NULL);
	return (str);
}

char		**ft_strsplit(const char *s, const char c)
{
	char	**tab;
	int		i;
	int		len;
	int		start;
	int		nb;

	i = 0;
	len = 0;
	start = 0;
	nb = ft_get_nb(s, c);
	if (!(tab = ft_strnew_2d(nb)))
		return (NULL);
	while (i < nb)
	{
		while (s[start] == c)
			start++;
		len = ft_get_len(s, c);
		tab[i] = ft_strsub(s, start, len);
		start += len;
		i++;
	}
	tab[i] = NULL;
	return (tab);
}
