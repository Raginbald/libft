/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:46:28 by graybaud          #+#    #+#             */
/*   Updated: 2014/02/02 18:50:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <libft.h>

char		*ft_strstr(char *s1, char *s2)
{
	if (!s1 || ft_strlen(s2) > ft_strlen(s1))
		return (NULL);
	else if (!ft_strncmp(s1, s2, ft_strlen(s2)))
		return (s1);
	else
		return (ft_strstr(s1 + 1, s2));
}
