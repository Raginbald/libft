/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 11:27:07 by graybaud          #+#    #+#             */
/*   Updated: 2015/01/24 11:12:32 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*copy;
	size_t	i;

	i = 0;
	if (!(copy = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	while (s[i] && i != len)
		copy[i++] = s[start++];
	copy[i] = '\0';
	return (copy);
}
