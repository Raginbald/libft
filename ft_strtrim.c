/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 16:45:10 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/03 11:16:30 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static size_t	ft_withoutspacelen(char const *s, size_t start, int end)
{
	size_t		len;

	len = ft_strlen(s);
	if (start && end)
		len = ft_strlen((s) + 1 - (end + start));
	return (len);
}

static int		ft_end(char const *s)
{
	int			end;

	end = ft_strlen(s) - 1;
	if (end <= 0)
		return (0);
	while (ft_iswhitespace(s[end]) && end >= 0)
		end--;
	return (end + 1);
}

static size_t	ft_start(char const *s)
{
	size_t		start;

	start = 0;
	while (ft_iswhitespace(s[start]))
		start++;
	if (s[start] == '\0')
		return (-1);
	else
		return (start);
}

char			*ft_strtrim(char const *s)
{
	char		*copy;
	size_t		start;
	int			end;
	size_t		len;

	copy = NULL;
	start = ft_start(s);
	end = ft_end(s);
	len = ft_withoutspacelen(s, start, end);
	if (!s)
		return (NULL);
	if (s)
	{
		if (start && !end)
			start = 0;
		copy = ft_strsub(s, start, len);
	}
	return (copy);
}
