/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabfree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/23 09:27:36 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/23 09:28:47 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_tabfree(char **tab)
{
	char	**cpy;

	if (tab)
	{
		cpy = tab;
		while (*cpy)
			ft_memfree(*cpy++);
	}
	ft_memfree(tab);
}
