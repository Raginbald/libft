/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftx_memalloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 08:44:39 by rthebaud          #+#    #+#             */
/*   Updated: 2014/01/07 10:35:59 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ftx_memalloc(size_t size)
{
	char	*b;

	if (!(b = (char *)malloc(size)))
		ftx_err("Memory allocation failed");
	ft_bzero(b, size);
	return (b);
}
